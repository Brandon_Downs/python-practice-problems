# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def find_second_largest(values):
#     if len(str(values)) == 0:
#         return None
#     if len(str(values)) == 1:
#         return None

#     for item in values:
#         if x > y:
#             return x
#         else:
#             return y




def find_second_largest(values):
    if len(values) <= 1:
        return None
    max_value = values[0]
    second_largest = None
    for value in values[1:]:
        if value > max_value:
            second_largest = max_value
            max_value = value
        elif second_largest is None or value > second_largest:
            second_largest = value
    return second_largest





values = (1, 3, 5, 6, 7)
print(find_second_largest(values))



#accepts a list of numerical values
#if empty, return none
#if only one value, return none
#returns second largest in list


#I need to get better with lists and identifying them. All this says is
#define function(input)
#if its 1 or 0 return none
#create list = values[0]
#second_largest = None
#So right now weve got the function, return none if 0/1
#created a list named max_value and assigned it values[0]
#set no value to second_largest but established it as a variable
#the for loop says, for every number in the values list [idk what [1:] is]
#if value> max value means if the new number is greater than 0,
#it is now the max value. after the loop is done it produces a new max value

# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
list1:  [1, 2, 3, 4]
list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.

def pairwise_add(list1, list2):
    results = []
    for value1, value2 in zip(list1, list2):
        results.append(value1 + value2)
    return results


print(pairwise_add(results))
    # list3 = map(sum, zip(list1, list2))
    # for item in list3:
    #     pieces = item.split(",")
    #     line_sum = 0
    #     for piece in pieces:
    #         value = int(piece)
    #         line_sum += value
    # return result_list


# list1: [100, 200, 300]
# list2: [ 10,   1, 180]


# print(pairwise_add(list1, list2))
# print()

# def list1: [100, 200, 300]
# def list2: [10, 1, 180]
# def list3: map(sum, zip(list1, list2))

# print(list3)


#Idk why the print result never works. Whatever.
#Works^^

# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def can_make_pasta(ingredients):
#     pass

#if true if ingredients are in list
#list will always have 3 items
#if list doesnt have flour eggs oil, return false


# pasta_ingredients = ["flour", "eggs", "oil"]
# ingredients = "flour"

# if "ingredients" in pasta_ingredients:
#     print(True)

ingredients = ("flour", "eggs", "oil")
# ingredients = "flour"


def can_make_pasta(ingredients):
    if (
        "flour" in ingredients
        and "eggs" in ingredients
        and "oil" in ingredients
    ):
        return True
    else:
        return False


if can_make_pasta(ingredients):
    print("You can make pasta!")
else :
    print("No pasta for you.")


#^Gottem

# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def minimum_value(value1, value2):
    #compares two values and identifies which is lower
    #
    #
    #return minimum of 2 values
    #if neither, return either
#has to return a value defined as the minimum
#print minimum value
#needs a test list for values
#value1 = [1, 2, 3, 4]
#value2 = [1, 3, 3, 5]


#code pulled from man:min() needs parameter
#so min(value1, value2) should produce which one is lower

#Without making this more difficult. Just start with list assign

# value1 = [123, 456, 789]
# value2 = [200, 300, 400]
# print("The minimum value is" , min(value1))
# print("The minimum value is" , min(value2))
#Had to use , its two separate strings, not vars. Got it.

#Wow, I overthought tf outta this

def minimum_value(value1, value2):
    if value1 < value2:
        return value1
    else:
        return value2

value1 = 7
value2 = 2
print(minimum_value(value1, value2))


#Working Answer!^^

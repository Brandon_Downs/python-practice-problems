# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

x, y = (1, 81)
def is_inside_bounds(x, y):
    if x >= 0 and y >= 0 and x <= 10 and y <= 10:
        print("That'll work.")
    else:
        print("I said 0-10")

is_inside_bounds(x, y)


#Almost had it, just had to draw out the info a little more.

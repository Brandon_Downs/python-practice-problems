# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_first_n_numbers(limit):
    if limit < 0:
        return None
    sum = 0
    for i in range(limit + 1):
        sum = sum + i
    return sum

# def sum_of_first_n_numbers(limit):
#     if limit < 0:                                       # solution
#         return None                                     # solution
#     sum = 0                                             # solution
#     for i in range(limit + 1):                          # solution
#         sum = sum + i                                   # solution
#     return sum


limit = 5
print(sum_of_first_n_numbers(limit))







#accepts a numerical limit and returns sum of numbers
#from 0 up to including limit

#if value of limit <= 0, return None
#so it needs to produce a range of numbers from 0 to the limit
#range(0, limit)?
#Works^

# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

# def calculate_average(values):
#     values = []
# #accepts a list of num values
#     if values() == None:
#         return None
#     average = sum(values)
# #returns average of the numbers
# #if list is empty, return none






def calculate_average(values):
    if len(values) == 0:
        return None
    sum = 0
    for item in values:
        sum = sum + item
    return sum / len(values)


values = [1, 2, 3, 4]
print(calculate_average(values))


#Make sure to call the function after creating it.
#For this one its 'calculate_average'
#Works^^

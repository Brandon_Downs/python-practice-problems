# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.

def translate(key_list, dictionary):
    result = []
    for key in key_list:
        result.append(dictionary.get(key))
    return result


glpat-sVpMfP_f2L8Hm1VKnGZx


#inputs keys "x", "y" ///////dictionary "x": "a", "y": "b"
#outputs "a", "b" or None if not in dict
#the function will have to take a key input and return the corresponding term from the dict
#Use the problem solving sheet. It helps
#Works^^

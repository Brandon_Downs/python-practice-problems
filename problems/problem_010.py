# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def is_divisible_by_3(number):
#     pass

#is_divisible_by_3 returns "fizz" if val is div by 3
#if not just return (number)
#number % 3 == 0 to test for divisibility

def is_divisible_by_3(number):
    if number % 3 == 0:
        print("Fizz")
    else :
        print(number)


number = 99
is_divisible_by_3(number)


#Either I'm finally getting this or im doing something extremely wrong

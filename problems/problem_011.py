# Complete the is_divisible_by_5 function to return the
# word "buzz" if the value in the number parameter is
# divisible by 5. Otherwise, just return the number.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_divisible_by_5(number):
    pass


#is this not the same as before but with 5 instead of 3? Code worked.


def is_divisible_by_5(number):
    if number % 5 == 0:
        print("Buzz")
    else :
        print(number)


number = 15
is_divisible_by_5(number)

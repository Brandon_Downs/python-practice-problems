# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

# def max_in_list(values):
#     list = []
#     for num in values:
#         if num > values:
#             return list.append(list)
#         elif list == None:
#             return None
#         else:
#             pass




def max_in_list(values):
    if len(values) == 0:
        return None

    max_value = values[0]
    for item in values:
        if item > max_value:
            max_value = item
    return max_value


values = (5, 2, 3, 4)
print(max_in_list(values))


#Dissect time.
#have to do if len(values) == 0 first so it doesnt have to be an elif or else
#max_value = values [] establishes an empty list and reference to list
#for loop to compare items in values list and append highest to max_value list
#then return max_value list (which will be the highest number after loop is run)

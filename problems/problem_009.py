# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def is_palindrome(word):
#     pass

#theres a portion of the manual that covers reverse
#find the code
#its variable[::-1]
#
word = "dog"

def is_palindrome(word):
    if word == word[::-1]:
        return(is_palindrome)

if is_palindrome(word):
    print("Yes")
else:
    print("No")

word = "dog"

is_palindrome(word)


#gottem
